#!/usr/bin/env python
# coding: utf-8

# In[1]:


# chargement des datasets 

import pandas as pd
formation= pd.read_csv('191119_TrainingStagev2.csv', sep=';')
session= pd.read_csv('191119_TrainingSessions.csv', sep=';')
Register= pd.read_csv('191119_TrainingRegister.csv', sep=';')
Cost = pd.read_csv('191119_TrainingStageCost.csv', sep=';')
Employee=pd.read_csv('191119_Employees.csv', sep=';')
# Bassiro Test 01
# test 2

# In[2]:


# création de la dimension session 

session.columns
session_dimension= session[['clientcode','state','address','durationhour','availableplaces']]
session_dimension
session_dimension = session_dimension.rename(columns={'clientcode': 'session_ID'})
session_dimension
#print('les dimensions de session_dimension sont:',session_dimension.shape)


# In[3]:


# creation de la dimension  formation 
formation.columns
formation_dimension= formation[['clientcode', 'name', 'duration','organism','state','target','defaultregistermode']]
formation_dimension
formation_dimension= formation_dimension.rename(columns={'clientcode': 'formation_ID'})
print('les dimensions de formation_dimension sont:',formation_dimension.shape)


# In[4]:


# creation de la dimension salarié 
Employee.columns
Salarie_dimension= Employee[['username','lastname','firstname','birthdate','sex','state']]
Salarie_dimension
Salarie_dimension.rename(columns={'username': 'Salarie_ID'})
Salarie_dimension= Salarie_dimension.rename(columns={'username': 'Salarie_ID'})
Salarie_dimension
print('les dimensions de Salarie_dimension sont:',Salarie_dimension.shape)


# In[5]:


# création des tables  fait 

# table de fait session par salarié  
   
    # renommer la table register
    
Register =Register.rename(columns={'clientcode':'session_ID','traineeusername':'Salarie_ID'})
Register
tableDeFait_se_sal= Register[['session_ID','Salarie_ID']]
#tableDeFait_se_sal.loc[tableDeFait_se_sal["session_ID"]!="", "nombre_session"] = 1  # ce code passe egalement
#df['Status'] = ["Senior" if s >=400 else "Junior" for s in df['Salary']] # code ou je me suis inspiré
tableDeFait_se_sal['nombre_session']= [1 if s!= '' else 0 for s in tableDeFait_se_sal['session_ID']]
print(tableDeFait_se_sal)
print('les dimensions de tableDeFait_se_sal sont :',Salarie_dimension.shape)


# In[6]:


# table de fait session par formation
   
    # renommer la table register
     
session.columns
tableDeFait_se_form= session[['clientcode','coursecode','startdate']]
tableDeFait_se_form= tableDeFait_se_form.rename(columns={'clientcode':'session_ID','coursecode':'formation_ID'})
tableDeFait_se_form.loc[tableDeFait_se_form["session_ID"]!="", "nombre_session"] = 1       
tableDeFait_se_form
print('les dimensions de tableDeFait_se_form sont :',Salarie_dimension.shape)


# In[7]:


Register.columns


# In[8]:


# pour exporter les tables 
#session_dimension,formation_dimension,Salarie_dimension,tableDeFait_se_sal,tableDeFait_se_form
session_dimension.to_csv(r'C:\Users\GGUEDE\Desktop\data set Total Training\data_set_travaille\session_dimension.csv ', index = False, header = True)
formation_dimension.to_csv(r'C:\Users\GGUEDE\Desktop\data set Total Training\data_set_travaille\formation_dimension.csv ', index = False, header = True)
Salarie_dimension.to_csv(r'C:\Users\GGUEDE\Desktop\data set Total Training\data_set_travaille\Salarie_dimension.csv ', index = False, header = True)
tableDeFait_se_sal.to_csv(r'C:\Users\GGUEDE\Desktop\data set Total Training\data_set_travaille\tableDeFait_se_sal.csv ', index = False, header = True)
tableDeFait_se_form.to_csv(r'C:\Users\GGUEDE\Desktop\data set Total Training\data_set_travaille\tableDeFait_se_form.csv ', index = False, header = True)


# In[9]:


formation_dimension


# In[11]:


# creation d'une colonne cle technique dans la dim formation

formation_dimension["Formation_IDT"]= formation_dimension["formation_ID"]
for i in formation_dimension.index:
    print(i)


# In[16]:


for i in formation_dimension.index:
    print(formation_dimension["formation_ID"].iloc[i])
    formation_dimension["Formation_IDT"].iloc[i]= formation_dimension["formation_ID"].iloc[i]


# In[ ]:


#
#for i in formation_dimension.index:
   # if formation_dimension["formation_ID"].iloc[i] =="Word - perfectionnement":
       # formation_dimension["Formation_IDT"].iloc[i]= 1


# In[ ]:


for i in formation_dimension.index:
    if formation_dimension["formation_ID"].iloc[i] =="Word - perfectionnement":
        formation_dimension["Formation_IDT"].iloc[i]= 1
    if formation_dimension["formation_ID"].iloc[i] =="Word - initiation":
        formation_dimension["Formation_IDT"].iloc[i]= 2
        


# In[21]:


import numpy as np
index_formation = np.unique(formation_dimension["formation_ID"])
index_formation[0]

for k in range (len(index_formation)):
    for i in formation_dimension.index:
        if formation_dimension["formation_ID"].iloc[i] ==index_formation[k]:
            formation_dimension["Formation_IDT"].iloc[i]= k


# In[23]:


formation_dimension["Formation_IDT"]


# In[24]:


index_formation[1575]


# In[26]:


formation_dimension["formation_ID"]


# In[27]:


formation_dimension


# In[28]:


#exporter la base 

formation_dimension.to_csv(r'C:\Users\GGUEDE\Desktop\data set Total Training\data_set_travaille\formation_dimension.csv ', index = False, header = True)


# In[41]:


# creation clé technique dans la base session

session_dimension["session_IDT"]= session_dimension["session_ID"]

session_dimension
import numpy as np
index_session = np.unique(session_dimension["session_ID"])
index_session[0]

for k in range (len(index_session)):
    for i in session_dimension.index:
        if session_dimension["session_ID"].iloc[i] ==index_session[k]:
            session_dimension["session_IDT"].iloc[i]= k
            
session_dimension
            
            


# In[42]:


#exporter la base 

session_dimension.to_csv(r'C:\Users\GGUEDE\Desktop\data set Total Training\data_set_travaille\session_dimension.csv ', index = False, header = True)


# In[48]:


# formation_dimension ajour d'une cle technique par une autre manière 

#dim_live_candidature["key_candidature"] =  dim_live_candidature.index
formation_dimension['key_ID']= formation_dimension.index
formation_dimension

# ajout d'une cle technique session par une autre manière 

session_dimension['key_IDSession']= session_dimension.index


# In[86]:


tableDeFait_se_form=tableDeFait_se_form[["session_ID","formation_ID","startdate","nombre_session"]]
tableDeFait_se_form


# In[96]:


# jointure de table 

dim_format_join_tb= formation_dimension[["formation_ID","key_ID"]]
dim_session_join_tb= session_dimension[["session_ID","key_IDSession"]]
# code jointure gauche pd.merge(df_a, df_b, on='subject_id', how='left')
# table de fait formation
pd.merge(tableDeFait_se_form, dim_format_join_tb, on='formation_ID', how='left')
table1 =pd.merge(tableDeFait_se_form, dim_session_join_tb, on='session_ID', how='left')
table2 =pd.merge(dim_format_join_tb, table1, on="formation_ID", how='right')
table2

# exporter la table de fait
#table2.to_csv(r'C:\Users\GGUEDE\Desktop\data set Total Training\data_set_travaille\New_tablefait2.csv ', index = False, header = True)


# In[107]:


# code optimisé pour les jointuresb
formation_dimension.columns

pd.merge(tableDeFait_se_form,formation_dimension[["formation_ID","key_ID"]], on='formation_ID', how='left')


# In[100]:


session_dimension
session_dimension.to_csv(r'C:\Users\GGUEDE\Desktop\data set Total Training\data_set_travaille\New_sessionDimension.csv ', index = False, header = True)
formation_dimension
formation_dimension.to_csv(r'C:\Users\GGUEDE\Desktop\data set Total Training\data_set_travaille\New_FormationDimension.csv ', index = False, header = True)

